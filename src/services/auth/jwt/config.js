import axios from 'axios';

export default axios.create({
  // baseURL: `http://g-axon.work/jwtauth/api/`, //YOUR_API_URL HERE
  baseURL: `http://127.0.0.1:8000/api/`,
  headers: {
    'Content-Type': 'application/json',
  },
});
