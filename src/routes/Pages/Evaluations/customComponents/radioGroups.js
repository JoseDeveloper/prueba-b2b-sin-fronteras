import React, { useEffect } from 'react';
import { makeStyles } from "@material-ui/core/styles";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import CheckIcon from '@material-ui/icons/Check';

const useStyles = makeStyles({
    questionLabel: {
        fontSize: '10px'
    },
    label: {
        fontSize: '0.8em'
    }

});

export default function RadioButtonsGroup({ questions }) {
    const classes = useStyles();
    const [value, setValue] = React.useState('');
    const [options, setOptions] = React.useState([]);

    useEffect(() => {
        const jsonData = questions.answers_decode;
        const found = jsonData.find( data => data.isCorrect === true );
        if(found) setValue(found.option);
        setOptions(jsonData);
    }, [questions]);

    const handleChange = (event) => {
        setValue(event.target.value);
    };


    return (
        <FormControl component="fieldset">
            <FormLabel className={classes.questionLabel} component="legend">{questions.question}</FormLabel>
            <RadioGroup aria-label="answers" name={questions.question} value={value} onChange={handleChange}>
                {options &&
                    options.map((item, i) => {
                        return (<FormControlLabel classes={{ label: classes.label }} value={item.option} control={<Radio />} label={item.option} />)
                    })
                }
            </RadioGroup>
        </FormControl>
    );
}