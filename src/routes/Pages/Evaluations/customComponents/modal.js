import React, { useEffect, useRef } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import AddIcon from '@material-ui/icons/Add';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import RadioButtonsGroup from "./radioGroups";
import QuestionsAdded from "./questionsAdded";

// Services.
import { evaluations } from '../../../../../src/services/database/evaluationsUser';


export default function FormDialog({ openDialog, dialogData, nSelected, closeDialog, actualizeData }) {
    const [open, setOpen] = React.useState(false);
    const [data, setData] = React.useState({});
    const [addedQuestions, setAddedQuestions] = React.useState([]);
    const questionsAdded = useRef(null);

    useEffect(() => {
        setOpen(openDialog);
        setData(dialogData);
        console.log('DIALOG DATA ',dialogData)
    }, [openDialog, dialogData]);

    const handleClose = () => {
        setData({});
        setOpen(false);
        closeDialog();
    };

    const addQuestion = (e) => {
        e.preventDefault();
        const { id } = data;
        let object = {
            evaluation_id: id,
            question: '',
            type: 1,
            answers: [],
            edited: 0,
        }

        setAddedQuestions([...addedQuestions, object])
    }

    const checkQuestion = (ns, question, options) => { 
        let tempArrObjects = addedQuestions;

        tempArrObjects[ns] = question;
        tempArrObjects[ns].answers = options;
        setAddedQuestions(tempArrObjects);
    }


    const save = async () => {
        const dataObject = questionsAdded.current.doneAll();
        const { nSelected: ns, questionObject: question, options } = dataObject;
        console.log('DATA OBJECT => ',dataObject);

        let tempArrObjects = addedQuestions;

        tempArrObjects[ns] = question;
        tempArrObjects[ns].answers = options;
        setAddedQuestions(tempArrObjects);

        const { id: evaluationId } = data;
        const response = await evaluations.saveDataEvaluation(evaluationId, tempArrObjects); 
        const { actualizedData } = response;
        actualizeData(actualizedData);
    }

    return (
        <div>
            <Dialog
                open={open}
                onClose={handleClose}
                fullWidth={true}
                maxWidth={'sm'}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Questions and answers</DialogTitle>
                <a href="#" onClick={addQuestion}> <AddIcon /> </a>
                <DialogContent>
                    <DialogContentText>
                        {data.evaluationName}
                    </DialogContentText>
                    {
                        data.questions && data.questions.map((item, i) => {
                            return (<RadioButtonsGroup questions={item} />)
                        })
                    }
                    <br></br>
                    {
                        addedQuestions && addedQuestions.map((item, i) => {
                            return (<><QuestionsAdded ref={questionsAdded} question={item} nSelected={i} checkQuestion={checkQuestion} /><br></br></>)
                        })
                    }
                    {/* <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Question text"
                        type="email"
                        fullWidth
                    /> */}
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
          </Button>
                    <Button onClick={save} color="primary">
                        Send
          </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}