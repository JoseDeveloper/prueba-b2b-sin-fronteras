import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import GridContainer from '../../../@jumbo/components/GridContainer';
import PageContainer from '../../../@jumbo/components/PageComponents/layouts/PageContainer';
import Box from '@material-ui/core/Box';
import IntlMessages from '../../../@jumbo/utils/IntlMessages';
import Grid from '@material-ui/core/Grid';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";

// Dialog.
import FormDialog from "./customComponents/modal";

// Services.
import { evaluations } from '../../../../src/services/database/evaluationsUser';

const breadcrumbs = [
  { label: 'Home', link: '/' },
  { label: 'Evaluations', isActive: true },
];

const useStyles = makeStyles({
  table: {
    minWidth: 650
  }
});


const Evaluations = () => {
  const classes = useStyles();
  const { authUser } = useSelector(({ auth }) => auth);

  const [evaluationsData, setEvaluations] = useState([]);
  const [openDialog, setOpenDialog] = useState(false);
  const [dialogData, setDialogData] = useState({});
  const [nSelected, setNSelected] = useState(0);

  useEffect(() => {

    async function getEvaluations() {
      const response = await evaluations.getEvaluations();
      const { data } = response;
      setEvaluations(data);
    }

    getEvaluations();
  }, []);

  const handleOpenDialog = (e, row, i) => {
    e.preventDefault();
    setOpenDialog(true);
    setDialogData(row);
    setNSelected(i);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
    setDialogData({});
  };

  const actualizeData = (data) => {
    setEvaluations(data);
    setOpenDialog(false);
    handleCloseDialog();
  }

  return (
    <>
      <PageContainer heading={<IntlMessages id="pages.evaluation" />} breadcrumbs={breadcrumbs}>
        <GridContainer>
          <Grid item xs={12}>
            <Box>
              <IntlMessages id="pages.evaluation.description" />
            </Box>
          </Grid>
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Date</TableCell>
                  <TableCell align="right">Title</TableCell>
                  <TableCell align="right">Score</TableCell>
                  <TableCell align="right">Actions</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {evaluationsData.map((row, i) => (
                  <TableRow key={row.created_at}>
                    <TableCell component="th" scope="row">
                      {row.created_at}
                    </TableCell>
                    <TableCell align="right">{row.name}</TableCell>
                    <TableCell align="right">{row.score}</TableCell>
                    <TableCell align="right">
                      <a href="#" onClick={(e) => handleOpenDialog(e, row, i)}><VisibilityIcon /></a>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </GridContainer>

      </PageContainer>
      <FormDialog
        openDialog={openDialog}
        dialogData={dialogData}
        nSelected={nSelected}
        closeDialog={handleCloseDialog}
        actualizeData = {actualizeData}
      />
    </>
  );
};

export default Evaluations;
