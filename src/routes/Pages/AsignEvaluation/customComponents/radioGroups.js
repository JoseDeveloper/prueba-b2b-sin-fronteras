import React, { useEffect } from 'react';
import { makeStyles } from "@material-ui/core/styles";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import CheckIcon from '@material-ui/icons/Check';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import NotInterestedOutlinedIcon from '@material-ui/icons/NotInterestedOutlined';

const useStyles = makeStyles({
    questionLabel: {
        fontSize: '16px'
    },
    label: {
        fontSize: '14px'
    },
    icon: {
        fontSize: '14px',
        paddingTop: '6px'
    },
    padding: {
        padding: '10px'
    }
});

export default function RadioButtonsGroup({ questions }) {
    const classes = useStyles();
    const [value, setValue] = React.useState('');
    const [options, setOptions] = React.useState([]);
    const [userAnswers, setUserAnswers] = React.useState([]);

    useEffect(() => {

        let jsonData = questions.answers_decode;
        const userDbAnswers = (questions.user_answers) ? questions.user_answers.user_answers : [];
        console.log('JSON DATA => ', userDbAnswers, jsonData)
        setOptions(jsonData);
        setUserAnswers(userDbAnswers);
    }, [questions]);

    const handleChange = (event) => {
        setValue(event.target.value);
    };

    return (
        <FormControl component="fieldset"  className={classes.padding}>
            <FormLabel className={classes.questionLabel} component="legend">{questions.question}</FormLabel>
            <RadioGroup aria-label="answers" name={questions.question} value={value} onChange={handleChange}>
                {options &&
                    options.map((item, i) => {
                        // const icon = (userAnswers && item.isCorrect === userAnswers[i].userResponse) ?
                        const icon = (userAnswers && userAnswers[i] && item.isCorrect === userAnswers[i].userResponse) ?
                            <CheckCircleOutlineIcon className={classes.icon} style={{ fill: "green" }} /> :
                            <NotInterestedOutlinedIcon className={classes.icon} style={{ fill: "red" }} />
                        if (userAnswers) {
                            console.log('USSSS', item, userAnswers[i]);
                        }
                        // return (<FormControlLabel value={item.option} control={<Radio />} label={item.option} />)
                        return (
                            <>
                                <span className={classes.label}>
                                    {item.option}
                                    {icon}
                                </span>
                            </>)
                    })
                }
            </RadioGroup>
        </FormControl>
    );
}